<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Paradise kindergarteen School</title>
    <link rel="shortcut icon" href="assets/img/brand.png" />
    <!-- Bootstrap -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">



</head>
<body>
<section id="menu">
    <div class="container.fluid navbar-fixed-top">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>


                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
                        <li><a href="#about">about</a></li>
                        <li><a href="students/index.php">View Students list</a></li>
                        <li><a href="students/create.php">Add Student</a></li>
                        <li><a href="courses/index.php">Veiw Courses</a></li>
                        <li><a href="courses/create.php">Add Courses</a></li>

                        <li><a href="#contact">contact</a></li>
                    </ul>


                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</section>
<section id="slider" class="">
    <div class="container.fluid">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="assets/img/4.jpg" alt="" />
                    <div class="carousel-caption">
                        <h2>our staff room</h2>
                    </div>
                </div>
                <div class="item">
                    <img src="assets/img/5.jpg" alt="business">
                    <div class="carousel-caption">
                        <h2>our campus</h2>
                    </div>
                </div>
                <div class="item">
                    <img src="assets/img/6.jpg" alt="business">
                    <div class="carousel-caption">
                        <h2>our administrator</h2>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>
<section id="about">
    <div class="container-fluid" style="background: green; min-height: 300px; padding: 80px 0px;">


           <div class="row">
               <div style="background: blue; margin-left:35px; margin-right: 20px; color: white; min-height: 200px;" class="col-md-3">

                           <div class="about-top text-center">
                               <h2>About us</h2>
                               <p class="text-justify">This is one of the best kindergarten School in the Mirpur area. The total number of students of our school is nearly 300. We have 12 well educated and dedicated teachers. Beside this, we have very skilled office management system to run the school smoothly.  </p>
                           </div>


               </div>
               <div style="background: blue; margin-right: 20px; color: white;  "  class="col-md-4">

                   <h2 style="font-weight: bold; font-size: 36px; min-height: 110px; " class="text-center">Admin Login<h2>
                           <div>
                               <form>
                                   <div class="form-group">
                                       <label for="exampleInputEmail1">User Name</label>
                                       <input type="email" class="form-control" id="exampleInputEmail1" placeholder="user name">
                                   </div>
                                   <div class="form-group">
                                       <label for="exampleInputPassword1">Password</label>
                                       <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                   </div>


                                   <button type="submit" class="btn btn-info">Submit</button>
                               </form>

                           </div>


               </div>

               <div style="background: blue; color: white;  "  class="col-md-4">
                   <h2 style="font-weight: bold; font-size: 36px; min-height: 110px; " class="text-center">Student Login<h2>
                           <div>
                               <form>
                                   <div class="form-group">
                                       <label for="exampleInputEmail1">User Name</label>
                                       <input type="email" class="form-control" id="exampleInputEmail1" placeholder="user name">
                                   </div>
                                   <div class="form-group">
                                       <label for="exampleInputPassword1">Password</label>
                                       <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                   </div>


                                   <button type="submit" class="btn btn-info">Submit</button>
                               </form>

                           </div>
               </div>

           </div>
    </div>
</section>

<section id="contact">

    <div class="container text-center">

        <div class="our-top">
            <h2>contact us</h2>
            <p>Mohammad Taher Ali is a high profile government officer. At present, he is working at Govt. Haraganga college, munshigonj. He is very enthesiastic person. He wants to be a professional web developer. So, He is taking a web development training at BITM under SEIP.</p>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="contact-content">
                    <i class="fa fa-phone"></i>
                    <h3>call us</h3>
                    <h4>+08801716090595</h4>
                </div>
            </div>
            <div class="col-md-3">
                <div class="contact-content">
                    <i class="fa fa-map-marker"></i>
                    <h3>office location</h3>
                    <h4>55/7, Pallabi, Mirpur, Dhaka</h4>
                </div>
            </div>
            <div class="col-md-3">
                <div class="contact-content">
                    <i class="fa fa-envelope"></i>
                    <h3>Email</h3>
                    <h4>paragartenschool@gmail.com</h4>
                </div>
            </div>
            <div class="col-md-3">
                <div class="contact-content">
                    <i class="fa fa-globe"></i>
                    <h3>website</h3>
                    <h4>www.paradiseschool.org</h4>
                </div>
            </div>
        </div>
    </div>

</section>

<section id="footer">
    <div class="container">
        <p class="text-center">&copy; All rights reserved to Paradise Kindergarten School</p>
    </div>
</section>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="assets/js/bootstrap.min.js"></script>
<!-- <script src="js/isotope.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
 <script src="js/isotope.function.js"></script><script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>


 <script>
 jQuery(document).ready(function( $ ) {
     $('.counter').counterUp({
         delay: 10,
         time: 1000
     });
 });
</script>-->
</body>
</html>